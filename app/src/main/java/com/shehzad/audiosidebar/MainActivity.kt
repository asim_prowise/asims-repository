package com.shehzad.audiosidebar

import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.marginEnd
import androidx.core.view.marginStart
import kotlin.math.roundToInt

class MainActivity : AppCompatActivity() {

    private var seekerStartMargin = 0
    private var seekerEndMargin = 0

    private lateinit var seeky: SeekBar
    private lateinit var lowLay: ConstraintLayout
    private lateinit var lowTxt: TextView
    private lateinit var startLine: View
    private lateinit var endLine: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        seeky = findViewById(R.id.mySekker)
        lowLay = findViewById(R.id.myLowestLayout)
        lowTxt = findViewById(R.id.last_volume_value)
        startLine = findViewById(R.id.firstLine)
        endLine = findViewById(R.id.secondLine)

        setSeekbarLimits(10, 90)

        seeky.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                lowTxt.text = progress.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })

    }

    private fun setSeekbarLimits(minSeekerVolume: Int, maxSeekerVolume: Int) {

        seeky.progress = (minSeekerVolume + maxSeekerVolume) / 2

        lowTxt.text = seeky.progress.toString()

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            seeky.min = minSeekerVolume
        }
        seeky.max = maxSeekerVolume

        Log.d("AsimPx", "Width: ${lowLay.layoutParams.width}")

        //converting width from px to dp
        val dpWidth = pxToDp(lowLay.layoutParams.width)
        Log.d("Asim", "Width: $dpWidth")

        // -14 to get full round seekBar thumb at both ends
        seekerStartMargin = (dpWidth * minSeekerVolume / 100) - 14
        when (minSeekerVolume) {
            0 -> startLine.visibility = View.GONE
            else -> startLine.visibility = View.VISIBLE
        }

        // -14 to get full round seekBar thumb at both ends
        seekerEndMargin = (dpWidth * (100 - maxSeekerVolume) / 100) - 14
        when (maxSeekerVolume) {
            100 -> View.GONE
            else -> endLine.visibility = View.VISIBLE
        }

        //Setting seekBar margin start and margin end
        (seeky.layoutParams as ViewGroup.MarginLayoutParams).apply {
            marginStart = dpToPx(seekerStartMargin)
            marginEnd = dpToPx(seekerEndMargin)
        }
        Log.d("AsimMargin", "${seeky.marginStart}  ${seeky.marginEnd}")

    }

    fun dpToPx(dp: Int): Int {
        val displayMetrics: DisplayMetrics = this.resources.displayMetrics
        return (dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
    }

    fun pxToDp(px: Int): Int {
        val displayMetrics: DisplayMetrics = this.resources.displayMetrics
        return (px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
    }

    fun asimDptoPx(dp: Int): Int {
        val density = this.resources.displayMetrics.density
        return (dp / density).roundToInt()
    }

    fun asimPxtoDp(px: Int): Int {
        val density = this.resources.displayMetrics.density
        return (px * density).roundToInt()
    }

}
